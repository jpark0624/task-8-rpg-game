package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.IRarity;

public class Uncommon implements IRarity {
    // Stat modifier
    private double powerModifier = 1.2;
    // Color for display purposes
    private String itemRarityColor = Color.GREEN;

    // Public properties
    @Override
    public double powerModifier() {
        return powerModifier;
    }

    @Override
    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
