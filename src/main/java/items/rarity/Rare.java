package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.IRarity;

public class Rare implements IRarity {
    // Stat modifier
    private double powerModifier = 1.4;
    // Color for display purposes
    private String itemRarityColor = Color.BLUE;

    // Public properties
    @Override
    public double powerModifier() {
        return powerModifier;
    }

    @Override
    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
