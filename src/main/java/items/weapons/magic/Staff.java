package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Staff extends Weapon {

    // Constructors
    public Staff() {
    }

    public Staff(double itemRarityModifier) {
        super(WeaponStatsModifiers.STAFF_MAGIC_MOD, itemRarityModifier);
    }
}
