package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Wand extends Weapon {

    // Constructors
    public Wand() {
    }

    public Wand(double itemRarityModifier) {
        super(WeaponStatsModifiers.WAND_MAGIC_MOD, itemRarityModifier);
    }
}
