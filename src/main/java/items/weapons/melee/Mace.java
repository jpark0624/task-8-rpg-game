package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Mace extends Weapon {

    // Constructors
    public Mace() {
    }

    public Mace(double itemRarityModifier) {
        super(WeaponStatsModifiers.MACE_ATTACK_MOD, itemRarityModifier);
    }
}
