package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Hammer extends Weapon {

    // Constructors
    public Hammer() {
    }

    public Hammer(double itemRarityModifier) {
        super(WeaponStatsModifiers.HAMMER_ATTACK_MOD, itemRarityModifier);
    }
}
