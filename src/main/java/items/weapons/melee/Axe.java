package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Axe extends Weapon {

    // Constructors
    public Axe() {
    }

    public Axe(double itemRarityModifier) {
        super(WeaponStatsModifiers.AXE_ATTACK_MOD, itemRarityModifier);
    }
}
