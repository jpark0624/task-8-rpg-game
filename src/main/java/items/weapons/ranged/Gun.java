package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Gun extends Weapon {

    // Constructors
    public Gun() {
    }

    public Gun(double itemRarityModifier) {
        super(WeaponStatsModifiers.GUN_ATTACK_MOD, itemRarityModifier);
    }
}
