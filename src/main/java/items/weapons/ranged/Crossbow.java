package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Crossbow extends Weapon {

    // Constructors
    public Crossbow() {
    }

    public Crossbow(double itemRarityModifier) {
        super(WeaponStatsModifiers.CROSSBOW_ATTACK_MOD, itemRarityModifier);
    }
}
