package main.java.items.armor.abstractions;

public interface IArmor {
    double getRarityModifier();
    double getHealthModifier();
    double getPhysRedModifier();
    double getMagicRedModifier();
}
