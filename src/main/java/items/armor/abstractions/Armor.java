package main.java.items.armor.abstractions;

import main.java.basestats.ArmorStatsModifiers;

public abstract class Armor implements IArmor {
    // Rarity
    public final double rarityModifier;
    public double healthModifier;
    public double physRedModifier;
    public double magicRedModifier;

    // Public properties
    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public double getHealthModifier() { return ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER; }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    }


    // Constructors
    public Armor(double itemRarity, double healthModifier, double physRedModifier, double magicRedModifier) {
        this.rarityModifier = itemRarity;
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
    }
}
