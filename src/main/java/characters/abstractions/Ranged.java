package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Ranged extends Character{
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Ranged;
    public final ArmorType ARMOR_TYPE = ArmorType.Mail;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;

}
