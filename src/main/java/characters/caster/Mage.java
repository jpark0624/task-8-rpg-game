package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;
/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Caster {

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;


    // Constructor
    public Mage() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Mage(DamagingSpell damagingSpell) {
        this.currentHealth = baseHealth;
        this.damagingSpell = damagingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    // Character behaviours
    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        return baseMagicPower * damagingSpell.getSpellDamageModifier() * weapon.getWeaponModifier() * weapon.getRarityModifier();
    }

}
