package main.java.encounter;

public class EncounterDifficulty {
    public static final double DIFFICULTY_EASY_MODIFIER = 0.75;
    public static final double DIFFICULTY_NORMAL_MODIFIER = 1;
    public static final double DIFFICULTY_HARD_MODIFIER = 1.25;
}
