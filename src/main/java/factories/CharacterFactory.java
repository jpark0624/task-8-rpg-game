package main.java.factories;
// Imports
import main.java.characters.Support.Druid;
import main.java.characters.Support.Priest;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.characters.melee.Paladin;
import main.java.characters.melee.Rogue;
import main.java.characters.melee.Warrior;
import main.java.characters.ranged.Ranger;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public Character getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Mage:
                return new Mage();
            case Druid:
                return new Druid();
            case Rogue:
                return new Rogue();
            case Priest:
                return new Priest();
            case Ranger:
                return new Ranger();
            case Paladin:
                return new Paladin();
            case Warlock:
                return new Warlock();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}
