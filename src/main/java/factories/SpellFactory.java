package main.java.factories;
// Imports
import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;
/*
 This factory exists to be responsible for creating new spells.
*/
public class SpellFactory {
    public Spell getSpell(SpellType spellType) {
        switch(spellType) {
            case ArcaneMissile:
                return new ArcaneMissile();
            case Barrier:
                return new Barrier();
            case ChaosBolt:
                return new ChaosBolt();
            case Regrowth:
                return new Regrowth();
            case Rapture:
                return new Rapture();
            case SwiftMend:
                return new SwiftMend();
            default:
                return null;
        }
    }
}
